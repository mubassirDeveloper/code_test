Thoughts about the code:

The code was professional and sophisticated, the good thing was the usage of PHP build-in functions like "empty, isset, etc" which reduces the compilation time and line of code as well.

Another thing the names of variables and definitions of functions using pascal case and real-life feel which could be easily understood by anyone.

The bad thing was the lack of DRY code, the repetition of the same task was seen multiple times in the code. Which could be done in another function with default values defined in the definition of the code. My suggestions are proper combination OPP plus functional programming to make the perfect out of all practices.